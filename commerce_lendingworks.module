<?php

define('commerce_lendingworks_WATCHDOG_TYPE', 'commerce_lendingworks');
define('commerce_lendingworks_SESSION_KEY', 'commerce_lendingworks_order_token');

/**
 * Implements hook_menu().
 */
function commerce_lendingworks_menu() {
  $items = array();

  $items['commerce_lendingworks/callback'] = array(
    'page callback' => 'commerce_lendingworks_handle_callback',
    'page arguments' => array(),
    'access callback' => true,
    'type' => MENU_CALLBACK,
  );

  // Add a menu item for fulfilling orders.
  $items['admin/commerce/orders/%commerce_order/payment/%commerce_payment_transaction/lendingworks-rf-fulfil'] = array(
    'title' => 'Fulfil',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_lendingworks_fulfil_form', 3, 5),
    'access callback' => 'commerce_lendingworks_fulfil_access',
    'access arguments' => array(3, 5),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 2,
  );

  return $items;
}

/**
 * Determines access to the fulfil form.
 *
 * @param $order
 *   The order the transaction is on.
 * @param $transaction
 *   The payment transaction object to be voided.
 *
 * @return bool
 *   TRUE or FALSE indicating void access.
 */
function commerce_lendingworks_fulfil_access($order, $transaction) {
  if ($transaction->payment_method !== 'commerce_lendingworks'
      || empty($transaction->remote_id)
      || $transaction->remote_status !== 'accepted') {
    return false;
  }

  // Allow access if the user can update this transaction.
  return commerce_payment_transaction_access('update', $transaction);
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_lendingworks_commerce_payment_method_info() {
  return array(
    'commerce_lendingworks' => array(
      'title' => t('Commerce Lending Works'),
      'short_title' => t('Commerce LW'),
      'display_title' => t('Finance with Lending Works'),
      'description' => t('Let customers finance their order using Lending Works.'),
      'active' => true,
      'terminal' => false,
      // Not technically 'offsite'.
      'offsite' => false,
    ),
  );
}

/**
 * Payment method settings form.
 *
 * @param $settings
 *   Default settings provided from rules
 *
 * @return array
 *   Settings form array
 */
function commerce_lendingworks_settings_form($settings) {
  $form = array();

  $envs = _commerce_lendingworks_environments();
  $envOpts = array();
  foreach ($envs as $key => $env) {
    $envOpts[$key] = $env['name'];
  }

  $form['description'] = array(
    '#markup' => <<< HTML
<p>Please contact <a href="mailto:partnerships@lendingworks.co.uk">partnerships@lendingworks.co.uk</a> if you need help with any of the below options.</p>
HTML
    ,
  );

  $overrideMsg = t(
    ' <strong>This setting is currently being overriden by your settings.php file.</strong>'
  );

  $hasEnvOverride = (
    variable_get('commerce_lendingworks_url_api') !== null
    || variable_get('commerce_lendingworks_url_js') !== null
  );

  $form['env'] = array(
    '#type' => 'select',
    '#title' => t('Environment'),
    '#description' => t(
      'Select the environment you want orders to go to. Use "Integration" for any testing.!override',
      array(
        '!override' => $hasEnvOverride ? $overrideMsg : '',
      )
    ),
    '#options' => $envOpts,
    '#default_value' => !empty($settings['env']) ? $settings['env'] : null,
    '#required' => true,
    '#disabled' => $hasEnvOverride,
  );

  $hasApiKeyOverride = (variable_get('commerce_lendingworks_api_key') !== null);

  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('The API key provided by Lending Works.!override', array(
      '!override' => $hasApiKeyOverride ? $overrideMsg : '',
    )),
    '#default_value' => !empty($settings['api_key']) ? $settings['api_key'] : '',
    '#required' => TRUE,
    '#disabled' => $hasApiKeyOverride,
  );

  return $form;
}

/**
 * Builds the checkout form.
 *
 * This initiates an order creation call to Lending Works and embeds the
 * resulting order token in the payment form.
 *
 * @param $payment_method
 * @param $pane_values
 * @param $checkout_pane
 * @param $order
 *
 * @return array
 */
function commerce_lendingworks_submit_form(
  $payment_method,
  $pane_values,
  $checkout_pane,
  $order
) {
  $form = array();
  $orderWrapper = entity_metadata_wrapper('commerce_order', $order);
  /** @noinspection PhpUndefinedFieldInspection */
  $orderTotal = $orderWrapper->commerce_order_total->value();

  $products = array();
  /** @noinspection PhpUndefinedFieldInspection */
  foreach ($orderWrapper->commerce_line_items as $delta => $liWrapper) {
    $productAmount = $liWrapper->commerce_unit_price->value();
    $products[] = array(
      'quantity' => $liWrapper->quantity->value(),
      'cost' => _commerce_lendingworks_bcdiv(
        $productAmount['amount'],
        100,
        5
      ),
      'description' => $liWrapper->commerce_product->title->value(),
    );
  }

  $settings = $payment_method['settings'];
  if (!array_key_exists('env', $settings)
      || !array_key_exists('api_key', $settings)) {
    _commerce_lendingworks_log_error(
      'The Lending Works Retail Finance integration is not configured, please check your payment method configuration.'
    );
    return $form;
  }

  $env = _commerce_lendingworks_environments($settings['env']);
  if ($env === null) {
    _commerce_lendingworks_log_error(
      'Could not find Lending Works environment: @env.',
      array(
        '@env' => $settings['env'],
      )
    );
    return $form;
  }

  $orderAmount = _commerce_lendingworks_bcdiv(
    $orderTotal['amount'],
    100,
    2
  );

  $apiKey = _commerce_lendingworks_get_api_key($settings);

  // Create an order so we can get a token.
  $orderToken = _commerce_lendingworks_create_order(
    $settings['env'],
    $apiKey,
    $orderAmount,
    $products
  );

  if ($orderToken === null) {
    _commerce_lendingworks_log_error(
      'Could not create a Lending Works order, please check the log for more details.'
    );
    return $form;
  }

  $modulePath = drupal_get_path('module', 'commerce_lendingworks');
  $checkoutJs = $env['js'];

  $form['#attached']['js'] = array(
    array(
      'data' => array(
        'commerceLendingWorks' => array(
          'orderToken' => $orderToken,
        ),
      ),
      'type' => 'setting',
    ),
    $checkoutJs => array(
      'type' => 'external'
    ),
    "{$modulePath}/js/commerce_lendingworks.js" => array(
      'preprocess' => false,
      'cache' => false,
    ),
  );

  $form['order_status'] = array(
    '#type' => 'hidden',
    '#default_value' => '',
  );

  $form['order_reference'] = array(
    '#type' => 'hidden',
    '#default_value' => '',
  );

  // To display validation errors.
  $form['errors'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="commerce-lendingworks-errors"></div>',
  );

  return $form;
}

function commerce_lendingworks_submit_form_submit(
  $payment_method,
  $pane_form,
  $pane_values,
  $order,
  $charge
) {
  // Reset session just in case.
  if (array_key_exists(commerce_lendingworks_SESSION_KEY, $_SESSION)) {
    unset($_SESSION[commerce_lendingworks_SESSION_KEY]);
  }

  $transaction = commerce_payment_transaction_new(
    'commerce_lendingworks',
    $order->order_id
  );
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $payment_method['settings']['stripe_currency'];
  $transaction->remote_status = $pane_values['order_status'];
  $transaction->remote_id = $pane_values['order_reference'];

  $cleanStatus = ucwords(str_replace(
    '_',
    ' ',
    $pane_values['order_status']
  ));
  $transaction->message = $cleanStatus;

  switch ($pane_values['order_status']) {
    case 'accepted':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      break;

    case 'approved':
    case 'referred':
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      break;

    default:
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      break;
  }

  $success = commerce_payment_transaction_save($transaction);
  if (!$success) {
    drupal_set_message(
      t('Our site is currently unable to process your finance application. Please contact the site administrator to complete your transaction.'),
      'error'
    );
    watchdog(
      commerce_lendingworks_WATCHDOG_TYPE,
      'commerce_payment_transaction_save returned false in saving a Lending Works Retail Finance transaction for order_id @order_id.',
      array('@order_id' => $transaction->order_id),
      WATCHDOG_ERROR
    );
  }
}

/**
 * Page callback handler for 'commerce_lendingworks/callback'.
 *
 * This ica
 */
function commerce_lendingworks_handle_callback() {
  $data = file_get_contents('php://input');
  $parsedData = null;

  if ($data !== null) {
    $parsedData = json_decode($data, true);

    if (json_last_error() !== JSON_ERROR_NONE) {
      watchdog(
        commerce_lendingworks_WATCHDOG_TYPE,
        'Data sent to Callback URL could not be parsed as JSON, error was @error, data was: @data.',
        array(
          '@error' => json_last_error(),
          '@data' => $data,
        ),
        WATCHDOG_WARNING
      );

      $parsedData = null;
    }
  }

  drupal_page_is_cacheable(false);
  drupal_add_http_header('Content-Type', 'application/json');
  $badRequestError = function ($msg = 'Bad Request') {
    print drupal_json_encode(array(
      'statusCode' => 400,
      'error' => 'Bad Request',
      'message' => $msg,
    ));
  };

  if ($parsedData === null) {
    watchdog(
      commerce_lendingworks_WATCHDOG_TYPE,
      'Callback URL accessed with no request body.',
      array(),
      WATCHDOG_WARNING
    );

    drupal_add_http_header('Status', '400 Bad Request');

    $badRequestError('A request body is required.');
    return;
  }

  $requiredKeys = array('reference', 'status');
  foreach ($requiredKeys as $key) {
    if (empty ($parsedData[$key])) {
      $badRequestError('The request body is invalid.');
      return;
    }
  }

  // Look up transaction via the remote ID.
  $remoteId = $parsedData['reference'];
  $query = new EntityFieldQuery;

  // Only get non-failed transactions.
  $result = $query
    ->entityCondition('entity_type', 'commerce_payment_transaction')
    ->propertyCondition('remote_id', $remoteId)
    ->propertyCondition('status', COMMERCE_PAYMENT_STATUS_FAILURE, '!=')
    ->propertyCondition('payment_method', 'commerce_lendingworks')
    ->propertyOrderBy('created', 'DESC')
    ->range(0, 1)
    ->execute();

  $transactionLoadError = function () use ($remoteId, $parsedData) {
    _commerce_lendingworks_log_error(
      "Could not find a transaction for remote ID '@id', tried to update status to '@status'.",
      array(
        '@id' => $remoteId,
        '@status' => $parsedData['status'],
      ),
      false
    );
  };

  // We don't return an error code since we don't want the webhook gateway to
  // keep retrying.
  if (empty($result['commerce_payment_transaction'])) {
    $transactionLoadError();
    return;
  }

  $result = reset($result['commerce_payment_transaction']);
  $transaction = commerce_payment_transaction_load($result->transaction_id);

  if (empty($transaction)) {
    $transactionLoadError();
    return;
  }


  $transaction->remote_status = $parsedData['status'];

  $cleanStatus = ucwords(str_replace(
    '_',
    ' ',
    $parsedData['status']
  ));
  $transaction->message = $cleanStatus;


  switch ($parsedData['status']) {
    case 'accepted':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;

      watchdog(
        LW_RF_CHECKOUT_WATCHDOG_TYPE,
        'Updated transaction @id on order @order_id to status success.',
        array(
          '@id' => $result->transaction_id,
          '@order_id' => $transaction->order_id,
        ),
        WATCHDOG_INFO
      );
      break;

    case 'fulfilled':
      if ($transaction->status !== COMMERCE_PAYMENT_STATUS_SUCCESS) {
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;

        watchdog(
          LW_RF_CHECKOUT_WATCHDOG_TYPE,
          'Updated transaction @id on order @order_id to status success.',
          array(
            '@id' => $result->transaction_id,
            '@order_id' => $transaction->order_id,
          ),
          WATCHDOG_INFO
        );
      }
      break;

    case 'expired':
    case 'declined':
    case 'cancelled':
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;

      watchdog(
        LW_RF_CHECKOUT_WATCHDOG_TYPE,
        'Updated transaction @id on order @order_id to status failure.',
        array(
          '@id' => $result->transaction_id,
          '@order_id' => $transaction->order_id,
        ),
        WATCHDOG_INFO
      );
      break;

    default:
      watchdog(
        LW_RF_CHECKOUT_WATCHDOG_TYPE,
        'Updated remote_status on transaction @id (order ID @order_id) to @status, did not change transaction status.',
        array(
          '@id' => $result->transaction_id,
          '@order_id' => $transaction->order_id,
          '@status' => $parsedData['status'],
        ),
        WATCHDOG_INFO
      );
  }


  commerce_payment_transaction_save($transaction);

  // Tell all other modules that we've received a callback request.
  module_invoke_all(
    'commerce_lendingworks_callback_process',
    $transaction,
    $data
  );

  print drupal_json_encode([
    'status' => 'success',
  ]);
}

/**
 * Implements hook_form().
 *
 * Allows a user to mark a payment as fulfilled.
 *
 * @param array $form
 * @param array $form_state
 * @param object $order
 * @param object $transaction
 *
 * @return array
 */
function commerce_lendingworks_fulfil_form($form, &$form_state, $order, $transaction) {
  $form_state['order'] = $order;
  $form_state['transaction'] = $transaction;

  // Load and store the payment method instance for this transaction.
  $payment_method = commerce_payment_method_instance_load(
    $transaction->instance_id
  );
  $form_state['payment_method'] = $payment_method;

  $form['markup'] = array(
    '#markup' => t('Are you sure that you want to mark this transaction as fulfilled?'),
  );

  $form = confirm_form($form,
    t('Are you sure that you want to mark this transaction as fulfilled?'),
    "admin/commerce/orders/{$order->order_id}/payment",
    '',
    t('Fulfil'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * @param array $form
 * @param array $form_state
 */
function commerce_lendingworks_fulfil_form_submit($form, &$form_state) {
  $transaction = $form_state['transaction'];
  $paymentMethod = $form_state['payment_method'];

  $transaction->remote_status = 'fulfilled';
  $transaction->message = 'Fulfilled';

  $apiKey = _commerce_lendingworks_get_api_key($paymentMethod['settings']);
  $success = _commerce_lendingworks_fulfil_transaction(
    $paymentMethod['settings']['env'],
    $apiKey,
    $transaction->remote_id
  );

  if (!$success) {
    _commerce_lendingworks_log_error(
      'Could not fulfil transaction @id, please check the log for more details.',
      array(
        '@id' => $transaction->id
      )
    );
  } else {
    drupal_set_message(
      t('Transaction successfully marked as fulfilled.')
    );
    commerce_payment_transaction_save($transaction);
  }

  $form_state['redirect'] = "admin/commerce/orders/{$form_state['order']->order_id}/payment";
}

/**
 * A list of all LW API environments.
 *
 * @param null $env If specified, will retrieve the URL for a particular env.
 *
 * @return array|mixed|null
 *   Either a list of environments, one particular env or NULL if nothing could
 *   be matched.
 */
function _commerce_lendingworks_environments($env = null) {
  $envs = array(
    'integration' => array(
      'name' => t('Integration (sandbox)'),
      'api' => 'https://integration.lendingworks.co.uk/api/v2',
      'js' => 'https://integration.secure.lendingworks.co.uk/checkout.js',
    ),
    'production' => array(
      'name' => t('Production'),
      'api' => 'https://www.lendingworks.co.uk/api/v2',
      'js' => 'https://secure.lendingworks.co.uk/checkout.js',
    ),
  );

  if ($env !== null) {
    if (array_key_exists($env, $envs)) {
      // Check for overrides.
      foreach (['api', 'js'] as $type) {
        $override = variable_get("commerce_lendingworks_url_{$type}");
        if ($override !== null) {
          $envs[$env][$type] = $override;
        }
      }

      return $envs[$env];
    }
    return null;
  }

  return $envs;
}

/**
 * Make an API request.
 *
 * @param string $env Environment key.
 * @param string $apiKey API key.
 * @param string $endpoint API endpoint.
 * @param string $requestBody Request body.
 *
 * @return array|null|false API response or FALSE if it failed.
 */
function _commerce_lendingworks_request($env, $apiKey, $endpoint, $requestBody) {
  $envOpts = _commerce_lendingworks_environments($env);
  if ($envOpts === null) {
    watchdog(
      commerce_lendingworks_WATCHDOG_TYPE,
      'Could not find Lending Works API environment: @env.',
      array(
        '@env' => $env,
      ),
      WATCHDOG_ERROR
    );
    return false;
  }

  $c = curl_init("{$envOpts['api']}/{$endpoint}");

  curl_setopt($c, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    "Authorization: RetailApiKey {$apiKey}",
  ]);

  curl_setopt($c, CURLOPT_POST, true);
  curl_setopt($c, CURLOPT_POSTFIELDS, $requestBody);
  curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

  $json = curl_exec($c);
  $code = (int) curl_getinfo($c, CURLINFO_RESPONSE_CODE);

  curl_close($c);

  $allowedCodes = [200, 202, 204];
  if (!in_array($code, $allowedCodes, true)) {
    watchdog(
      commerce_lendingworks_WATCHDOG_TYPE,
      'The Lending Works API returned a non-20x HTTP code, code was: @code, response was: @res.',
      array(
        '@code' => $code,
        '@res' => (string) $json,
      ),
      WATCHDOG_ERROR
    );
    return false;
  }

  $res = json_decode($json, true);
  if ($res === null) {
    watchdog(
      commerce_lendingworks_WATCHDOG_TYPE,
      'Could not decode API response.',
      array(),
      WATCHDOG_ERROR
    );
    return false;
  }

  return $res;
}

/**
 * Call the Lending Works API to create an order.
 *
 * @param string $env Environment key.
 * @param string $apiKey API key.
 * @param float $amount Order amount.
 * @param array $products An array of products with the following keys:
 *  - cost (float)
 *  - quantity (int)
 *  - description (string)
 *
 * @return string|null Either an order token or NULL if an error occurred.
 */
function _commerce_lendingworks_create_order($env, $apiKey, $amount, array $products) {
  $data = json_encode(
    array(
      'amount' => $amount,
      'products' => $products,
    ),
    JSON_PRESERVE_ZERO_FRACTION | JSON_PRETTY_PRINT
  );
  $hash = md5($data);

  // Check if we already have an order token for this request.
  if (!empty($_SESSION[commerce_lendingworks_SESSION_KEY])
      && !empty($_SESSION[commerce_lendingworks_SESSION_KEY][$hash])) {
    return $_SESSION[commerce_lendingworks_SESSION_KEY][$hash];
  }

  if (json_last_error() !== JSON_ERROR_NONE) {
    watchdog(
      commerce_lendingworks_WATCHDOG_TYPE,
      'Failed encoding order creation request body: ' . json_last_error_msg(),
      array(),
      WATCHDOG_ERROR
    );
    return null;
  }

  $res = _commerce_lendingworks_request($env, $apiKey, 'orders', $data);
  if ($res === null || $res === false) {
    return null;
  }

  if (!array_key_exists('token', $res)) {
    watchdog(
      commerce_lendingworks_WATCHDOG_TYPE,
      'API response was invalid, got: @res',
      array(
        '@res' => json_encode($data),
      ),
      WATCHDOG_ERROR
    );
    return null;
  }

  $_SESSION[commerce_lendingworks_SESSION_KEY] = [
    $hash => $res['token']
  ];

  return $res['token'];
}

/**
 * Fulfil an order transaction.
 *
 * @param string $env Environment key.
 * @param string $apiKey API key.
 * @param string $reference Loan application reference.
 *
 * @return bool Fulfilment success.
 */
function _commerce_lendingworks_fulfil_transaction($env, $apiKey, $reference) {
  $data = json_encode(
    array(
      'reference' => $reference,
    ),
    JSON_PRESERVE_ZERO_FRACTION | JSON_PRETTY_PRINT
  );

  if (json_last_error() !== JSON_ERROR_NONE) {
    watchdog(
      commerce_lendingworks_WATCHDOG_TYPE,
      'Failed encoding order fulfil request body: ' . json_last_error_msg(),
      array(),
      WATCHDOG_ERROR
    );
    return false;
  }

  $res = _commerce_lendingworks_request(
    $env,
    $apiKey,
    'loan-requests/fulfill',
    $data
  );

  if ($res === false) {
    return false;
  }

  return true;
}

/**
 * Log an error to screen and watchdog.
 *
 * @param string $msg
 * @param array $args
 * @param bool $printMsg
 */
function _commerce_lendingworks_log_error($msg, array $args = array(), $printMsg = true) {
  if ($printMsg) {
    drupal_set_message(t($msg, $args), 'error', FALSE);
  }
  watchdog(
    commerce_lendingworks_WATCHDOG_TYPE,
    $msg,
    $args,
    WATCHDOG_ERROR
  );
}

/**
 * A wrapper around bcdiv() that works even if ext-bcmath is not installed.
 *
 * @param int|string|float $leftOp Left operand.
 * @param int|string|float $rightOp Right operand.
 * @param int $precision Decimal precision.
 *
 * @return float
 */
function _commerce_lendingworks_bcdiv($leftOp, $rightOp, $precision = 2) {
  // Use bcmath if possible.
  if (extension_loaded('bcmath')) {
    return (float) bcdiv(
      (string) $leftOp,
      (string) $rightOp,
      $precision
    );
  }

  return round((float) $leftOp / (float) $rightOp, $precision);
}

/**
 * Get API key from the payment method's settings.
 *
 * @param array $settings Payment method settings.
 *
 * @return string
 */
function _commerce_lendingworks_get_api_key(array $settings) {
  $apiKey = $settings['api_key'];
  // Check for possible overrides.
  $apiKeyOverride = variable_get('commerce_lendingworks_api_key');
  if ($apiKeyOverride !== null) {
    $apiKey = $apiKeyOverride;
  }

  return $apiKey;
}
