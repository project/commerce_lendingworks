/**
 * @file
 * Display checkout popup and handle completion.
 */

(function ($) {
  Drupal.behaviors.lendingWorksCheckout = {
    attach: function (context, settings) {
      if (!('commerceLendingWorks' in settings) || 'loaded' in settings.commerceLendingWorks) {
        return;
      }

      if (!('orderToken' in settings.commerceLendingWorks)
        || settings.commerceLendingWorks.orderToken === null) {
        $('.commerce-lendingworks-errors').html(
          '<div class="messages error">Communication error, please try a different payment method.</div>'
        );
        return;
      }

      $('body').delegate('#edit-continue', 'click', function(event) {
        var $paymentSelectory = $("input[value*='commerce_lendingworks|']");
        if (!$paymentSelectory.length || $paymentSelectory.is(':checked')) {
          event.preventDefault();
          var $this = $(this);

          // Prevent duplicate submissions.
          if ($this.hasClass('commerce-lendingworks-processing')) {
            return false;
          }
          $this.addClass('commerce-lendingworks-processing');

          var $form = $("#edit-continue").closest("form");
          var $submitButtons = $form.find('.checkout-continue');
          $submitButtons.attr("disabled", "disabled");

          var checkoutHandler = LendingWorksCheckout(
            settings.commerceLendingWorks.orderToken,
            window.location.href,
            function (status, reference) {
              if (['accepted', 'referred'].indexOf(status) !== -1) {
                $("input[name='commerce_payment[payment_details][order_reference]']").val(reference);
                $("input[name='commerce_payment[payment_details][order_status]']").val(status);

                // Trigger form.
                var $trigger = $('.form-submit.commerce-lendingworks-processing').eq(0);
                var $newTrigger = $("<input type='hidden' />")
                  .attr('name', $trigger.attr('name'))
                  .attr('value', $trigger.attr('value'));
                $form.append($newTrigger);
                $form.get(0).submit($form);
              }

              if (['approved', 'declined', 'cancelled'].indexOf(status) !== -1) {
                $('.checkout-processing').hide();
              }

              if (status === 'declined') {
                $('.commerce-lendingworks-errors').html(
                  '<div class="messages error">Please try a different payment method.</div>'
                );
              } else if (status === 'cancelled') {
                $('.commerce-lendingworks-errors').html(
                  '<div class="messages warning">Your finance application has not been submitted, please try again or choose a different payment method.</div>'
                );
              }

              $this.removeClass('commerce-lendingworks-processing');
              $submitButtons.removeAttr('disabled');
            }
          );
          checkoutHandler();
        }
      });
    }
  }
})(jQuery);
